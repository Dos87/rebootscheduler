![RebootScheduler.jpg](https://bitbucket.org/repo/MpLeoK/images/4161521282-RebootScheduler.jpg)

![RebootSchedulerTrayNotification.jpg](https://bitbucket.org/repo/MpLeoK/images/2400587680-RebootSchedulerTrayNotification.jpg)

# RebootScheduler #

[Скачать](https://bitbucket.org/Dos87/rebootscheduler/downloads/RebootScheduler.zip)

Приложение для удобной перезагрузки Windows по расписанию.
В общем, это просто графический интерфейс для утилиты командной строки shutdown -r -t

* Не требует установки
* Можно запускать из сетевой папки
* Не оставляет мусора в системе
* Работает в Windows Core и Windows Hyper-V Server

Для работы приложения необходим .NET 4.0

The MIT License (MIT)
Copyright (c) 2016 Dmitriy Avdeev

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.