﻿using System;
using System.ComponentModel;
using System.Timers;

namespace RebootScheduler
{
    public class Ticker : INotifyPropertyChanged
    {
        public Ticker()
        {
            var timer = new Timer();
            timer.Interval = 1000; // 1 second updates
            timer.Elapsed += Timer_Elapsed;
            timer.Start();
        }

        public DateTime Now => DateTime.Now;

        private void Timer_Elapsed(object sender, ElapsedEventArgs e)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Now"));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
