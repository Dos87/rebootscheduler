﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace RebootScheduler
{
    public class ButtonTextConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            //0 - date, 1 - time
            if (values.Length != 2 || values[0] == null || values[1] == null)
                return "Укажите дату и время для перезагрузки";

            var d = (DateTime)values[0];
            var t = (DateTime)values[1];
            var dt = new DateTime(d.Year, d.Month, d.Day, t.Hour, t.Minute, t.Second);
            var timeToReboot = dt - DateTime.Now;

            if (timeToReboot.TotalMinutes < 0)
                return "Перезагрузка не может начаться раньше текущего времени!";

            return $"Перезагрузить компьютер через {(timeToReboot.TotalDays < 1 ? "" : (int)timeToReboot.TotalDays + " д. ")}{timeToReboot.Hours} ч. {timeToReboot.Minutes} мин.";
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            return null;
        }
    }
}
