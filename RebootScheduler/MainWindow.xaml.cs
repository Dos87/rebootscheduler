﻿using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Threading;
using System.Windows;

namespace RebootScheduler
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void OnMainButtonClick(object sender, RoutedEventArgs e)
        {
            if (Calend.SelectedDate == null)
                return;
            var dateTime = Calend.SelectedDate.Value.Add(Clock.Time.TimeOfDay);
            var secondsToReboot = (int)(dateTime - DateTime.Now).TotalSeconds;
            if (secondsToReboot < 0)
            {
                MessageBox.Show("Дата и время перезагрузки должно быть больше настоящего времени!", "Внимание!", MessageBoxButton.OK, MessageBoxImage.Exclamation);
                return;
            }
            var comment = Comment.Text;
            var re = new ManualResetEvent(false);
            ThreadPool.QueueUserWorkItem(state =>
            {
                var process = new Process();
                var startInfo = new ProcessStartInfo
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    FileName = "shutdown.exe",
                    UseShellExecute = true,
                    Verb = "runas",
                    Arguments = $"-r -t {secondsToReboot}"
                };
                if (!string.IsNullOrWhiteSpace(comment))
                {
                    comment += $"\nВремя перезагрузки: {dateTime.ToString("dd.MM.yyyyг. в HH:mm")}";
                    startInfo.Arguments += $" -c \"{comment}\"";
                }

                process.StartInfo = startInfo;
                try
                {
                    process.Start();
                }
                catch (Win32Exception exc)
                {
                    //1223 - The operation was canceled by the user.
                    if (exc.NativeErrorCode != 1223)
                        throw;
                }
                finally
                {
                    re.Set();
                }
            });
            re.WaitOne();
            Application.Current.Shutdown();
        }
    }
}
